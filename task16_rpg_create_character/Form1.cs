﻿using System;
using System.IO;
using System.Windows.Forms;
using task16_rpg;

namespace task16_rpg_create_character
{
    public partial class Form1 : Form
    {
        public string path = @"C:\Users\cthenn\Documents\experis\gitlab\accelerateexperistasks_week2\task16_rpg_create_character\new_character.txt";
      
        public Form1()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Character wiz = new Wizard(textBox1.Text);
            label14.Text = wiz.type;
            label12.Text = wiz.hp;
            label11.Text = wiz.energy;
            label10.Text = wiz.armor;
            label9.Text = wiz.attack;
            label8.Text = wiz.move;
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            Character war = new Warrior(textBox1.Text);
            label14.Text = war.type;
            label12.Text = war.hp;
            label11.Text = war.energy;
            label10.Text = war.armor;
            label9.Text = war.attack;
            label8.Text = war.move;
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            Character thief = new Thief(textBox1.Text);
            label14.Text = thief.type;
            label12.Text = thief.hp;
            label11.Text = thief.energy;
            label10.Text = thief.armor;
            label9.Text = thief.attack;
            label8.Text = thief.move;
        }
        
        private void Button4_Click(object sender, EventArgs e)
        {
            //CREATE/SAVE
            if (textBox1.Text.Length > 0)
            {
                SaveCharToFile();
                this.Close();
            } else
            {
                MessageBox.Show("No character name entered.", "ERROR",
                MessageBoxButtons.OK, MessageBoxIcon.None);
            }
        }

        private void SaveCharToFile()
        {
            if (File.Exists(path))
            {
                File.Delete(path);
            }

            using (StreamWriter sw = new StreamWriter(path))
            {
                sw.WriteLine($"Character name: { textBox1.Text}");
                sw.WriteLine($"Class: { label14.Text}");
                sw.WriteLine($"{label3.Text} { label12.Text}");
                sw.WriteLine($"{label4.Text} { label11.Text}");
                sw.WriteLine($"{label5.Text} { label10.Text}");
                sw.WriteLine($"{label6.Text} { label9.Text}");
                sw.WriteLine($"{label7.Text} { label8.Text}");
            }
        }
    }
}
