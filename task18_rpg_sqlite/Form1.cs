﻿using System;
using System.IO;
using System.Windows.Forms;
using task16_rpg;
using System.Data.SQLite;
using System.Data;

namespace task16_rpg_create_character
{
    public partial class Form1 : Form
    {
        public string path = @"C:\Users\cthenn\Documents\experis\gitlab\accelerateexperistasks_week2\task16_rpg_create_character\new_character.txt";

        public Form1()
        {
            InitializeComponent();
            PopulateListbox();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Character wiz = new Wizard(textBox1.Text);
            label14.Text = wiz.type;
            label12.Text = wiz.hp;
            label11.Text = wiz.energy;
            label10.Text = wiz.armor;
            label9.Text = wiz.attack;
            label8.Text = wiz.move;
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            Character war = new Warrior(textBox1.Text);
            label14.Text = war.type;
            label12.Text = war.hp;
            label11.Text = war.energy;
            label10.Text = war.armor;
            label9.Text = war.attack;
            label8.Text = war.move;
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            Character thief = new Thief(textBox1.Text);
            label14.Text = thief.type;
            label12.Text = thief.hp;
            label11.Text = thief.energy;
            label10.Text = thief.armor;
            label9.Text = thief.attack;
            label8.Text = thief.move;
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            //CREATE/SAVE
            if (textBox1.Text.Length > 0)
            {
                SaveCharToFile();
                ConnectionManager();
                PopulateListbox();
                textBox1.Text = "";
                //this.Close();
            }
            else
            {
                MessageBox.Show("No character name entered.", "ERROR",
                MessageBoxButtons.OK, MessageBoxIcon.None);
            }
        }

        private void SaveCharToFile()
        {
            if (File.Exists(path))
            {
                File.Delete(path);
            }

            using (StreamWriter sw = new StreamWriter(path))
            {
                sw.WriteLine($"Character name: { textBox1.Text}");
                sw.WriteLine($"Class: { label14.Text}");
                sw.WriteLine($"{label3.Text} { label12.Text}");
                sw.WriteLine($"{label4.Text} { label11.Text}");
                sw.WriteLine($"{label5.Text} { label10.Text}");
                sw.WriteLine($"{label6.Text} { label9.Text}");
                sw.WriteLine($"{label7.Text} { label8.Text}");
            }
        }

        private void ConnectionManager()
        {
            SQLiteConnection conn = new SQLiteConnection("Data Source = character.db; Version = 3; New = True; Compress = True; ");

            try
            {
                conn.Open();

                SQLiteCommand insert_cmd1 = conn.CreateCommand();
                SQLiteCommand insert_cmd2 = conn.CreateCommand();
                SQLiteCommand select_cmd = conn.CreateCommand();

                select_cmd.CommandText = $"SELECT ClassId FROM Class WHERE ClassType = '{label14.Text}'";

                SQLiteDataReader sql_reader1;

                sql_reader1 = select_cmd.ExecuteReader();
                while (sql_reader1.Read())
                {
                    string result = sql_reader1.GetInt32(0).ToString();

                    insert_cmd1.CommandText = $"INSERT INTO Character(Name, ClassId) VALUES('{textBox1.Text}', {result});";
                    insert_cmd1.ExecuteNonQuery();

                    insert_cmd2.CommandText = $"INSERT INTO Stats(Health, Energy, Armor, Attack, Move, ClassId)" +
                        $" VALUES('{label12.Text}', '{label11.Text}', '{label10.Text}', '{label9.Text}', '{label8.Text}', {result});";
                    insert_cmd2.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                conn.Close();
            }


        }

        private void PopulateListbox()
        {
            SQLiteConnection conn = new SQLiteConnection("Data Source = character.db; Initial Catalog = Character; Integrated Security = True");

            try
            {
                conn.Open();
                DataSet ds = new DataSet();
                SQLiteDataAdapter adapter = new SQLiteDataAdapter(
                "SELECT Name from Character", conn);
                adapter.Fill(ds);
                this.listBox1.DataSource = ds.Tables[0];
                this.listBox1.DisplayMember = "Name";
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }

        private void Button5_Click(object sender, EventArgs e)
        {
            if (textBox2.Text.Length > 0)
            {
                SQLiteConnection conn = new SQLiteConnection("Data Source = character.db; Version = 3; New = True; Compress = True; ");

                string nametext = listBox1.GetItemText(listBox1.SelectedItem);

                //MessageBox.Show(nametext);
                try
                {
                    conn.Open();
                    SQLiteCommand update_cmd1 = conn.CreateCommand();

                    update_cmd1.CommandText = $"UPDATE Character SET Name = '{textBox2.Text}' WHERE Name = '{nametext}';";
                    update_cmd1.ExecuteNonQuery();
                }
                catch (Exception ex)
                {

                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    conn.Close();
                }
            }
            else
            {
                MessageBox.Show("Enter new name.", "ERROR",
                MessageBoxButtons.OK, MessageBoxIcon.None);
            }

            textBox2.Text = "";
            PopulateListbox();
        }

        private void Button6_Click(object sender, EventArgs e)
        {
            SQLiteConnection conn = new SQLiteConnection("Data Source = character.db; Version = 3; New = True; Compress = True; ");

            string nametext = listBox1.GetItemText(listBox1.SelectedItem);

            try
            {
                conn.Open();
                SQLiteCommand update_cmd1 = conn.CreateCommand();

                update_cmd1.CommandText = $"DELETE FROM Character WHERE Name = '{nametext}';";
                update_cmd1.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
            finally
            {
                conn.Close();
            }

            PopulateListbox();
        }
    }
}
 
