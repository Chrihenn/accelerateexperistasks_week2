﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace task15_opening_file
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            string fileName = @"C:\Users\cthenn\Documents\experis\gitlab\accelerateexperistasks_week2\task15_opening_file\testfile_open.txt";
            var process = new System.Diagnostics.Process();

            process.StartInfo = new System.Diagnostics.ProcessStartInfo()
            {
                UseShellExecute = true, FileName = fileName
            };

            process.Start();
        }
    }
}
