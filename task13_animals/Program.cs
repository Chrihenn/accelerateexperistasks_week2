﻿using System;
using System.Collections.Generic;
using task13_animals.Animals;

namespace task13_animals
{
    class Program
    {
        static void Main(string[] args)
        {

            List<Animal> animals = new List<Animal>();

            Animal cat = new Cat("cat");
            Animal deer = new Deer("deer");
            Animal bird = new Bird("bird");

            animals.Add(cat);
            animals.Add(deer);
            animals.Add(bird);

            cat.CheckMovements();
            deer.CheckMovements();
            bird.CheckMovements();

            foreach ( Animal animal in animals)
            {
                Console.WriteLine("------------------------------------");
                animal.PrintMovements();
                animal.PrintRandomMovement();
                Console.WriteLine();
                Console.WriteLine("------------------------------------");
            }

            
            
        }
    }
}
