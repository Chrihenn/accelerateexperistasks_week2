﻿using System;
using System.Collections.Generic;
using System.Text;

namespace task13_animals
{
    class Animal
    {
        string name;
        public List<string> movements { get; set; }
        public List<string> isMoving { get; set; }

        public Animal(string name)
        {
            this.name = name;
            movements = new List<string>();
            isMoving = new List<string>();
        }

        public void PrintMovements()
        {
            Console.Write($"The {name} can: ");
            for (int i = 0; i < movements.Count; i++)
            {
                Console.Write($"{movements[i]}, ");
            }
        }

        public void PrintRandomMovement()
        {
            Random rnd = new Random();
            int index = rnd.Next(isMoving.Count);
            Console.Write($"\nThe {name} is {isMoving[index]}");
        }

        public void CheckMovements()
        {
            if (this.GetType().GetInterface("IFlyable") != null)
            {
                movements.Add("fly");
                isMoving.Add("flying");
            }

            if (this.GetType().GetInterface("IClimbable") != null)
            {
                movements.Add("climb");
                isMoving.Add("climbing");
            }

            if (this.GetType().GetInterface("IWalkable") != null)
            {
                movements.Add("walk");
                isMoving.Add("walking");
            }

            if (this.GetType().GetInterface("IRunnable") != null)
            {
                movements.Add("run");
                isMoving.Add("running");
            }
        }


    }
}
