﻿using System;
using System.Collections.Generic;
using System.Text;
using task13_animals.Movements;

namespace task13_animals.Animals
{
    class Deer : Herbivore, IWalkable, IRunnable
    {
        public Deer(string name) : base(name)
        {
        }

    }
}
