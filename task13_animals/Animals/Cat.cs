﻿using System;
using System.Collections.Generic;
using System.Text;
using task13_animals.Movements;

namespace task13_animals.Animals
{
    class Cat : Carnivore, IWalkable, IClimbable, IRunnable
    {
        public Cat(string name) : base(name)
        {
        }
    }
}
