﻿using System;
using task13_animals.Movements;

namespace task13_animals.Animals
{
    class Bird : Omnivore, IWalkable, IFlyable
    {
        public Bird(string name) : base(name)
        {
        }
    }
}
