﻿
namespace task16_rpg
{
    public class Warrior : Character
    {
        static string war_hp = "100";
        static string war_energy = "100";
        static string war_armor = "100";
        static string war_attack = "sword";
        static string war_move = "sprint";
        static string war_type = "WARRIOR";
        public Warrior(string name) :
            base(name, war_hp, war_energy, war_armor, war_attack, war_move, war_type)
        {
        }
    }
}
