﻿
namespace task16_rpg
{
    public class Thief : Character
    {
        static string thief_hp = "50";
        static string thief_energy = "300";
        static string thief_armor = "50";
        static string thief_attack = "martial arts";
        static string thief_move = "sneak";
        static string thief_type = "THIEF";

        public Thief(string name) : 
            base(name, thief_hp, thief_energy, thief_armor, thief_attack, thief_move, thief_type)
        {

        }
    }
}
