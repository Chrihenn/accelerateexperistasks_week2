﻿
namespace task16_rpg
{
    public class Wizard : Character
    {
        static string wiz_hp = "200";
        static string wiz_energy = "100";
        static string wiz_armor = "50";
        static string wiz_attack = "spells";
        static string wiz_move = "levitate";
        static string wiz_type = "WIZARD";
        public Wizard(string name) :
            base(name, wiz_hp, wiz_energy, wiz_armor, wiz_attack, wiz_move, wiz_type)
        {

        }
    }
}
