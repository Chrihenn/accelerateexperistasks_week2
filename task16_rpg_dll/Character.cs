﻿
namespace task16_rpg
{
    public class Character
    { 
        public string name;
        public string hp;
        public string energy;
        public string armor;
        public string attack;
        public string move;
        public string type;
        
        public string Name { get => name; set => name = value; }
        public string Hp { get => hp; set => hp = value; }
        public string Energy { get => energy; set => energy = value; }
        public string Armor { get => armor; set => armor = value; }
        public string Attack { get => attack; set => attack = value; }
        public string Move { get => move; set => move = value; }
        public string Type { get => type; set => type = value; }
        
        public Character(string name, string hp, string energy, string armor, string attack, string move, string type)
        {
            Name = name;
            Hp = hp;
            Energy = energy;
            Armor = armor;
            Attack = attack;
            Move = move;
            Type = type;
        }
    }


}
