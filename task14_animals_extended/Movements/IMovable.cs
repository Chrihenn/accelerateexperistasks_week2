﻿using System;
using System.Collections.Generic;
using System.Text;

namespace task13_animals.Movements
{
    interface IMovable
    {
        void CheckMovements();
    }
}
