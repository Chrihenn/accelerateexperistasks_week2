﻿using System;
using System.Collections.Generic;

namespace task13_animals
{
    class Animal : IComparable<Animal>
    {
        string name;
        public List<string> Movements { get; set; }
        public List<string> IsMoving { get; set; }

        public Animal(string name)
        {
            this.name = name;
            Movements = new List<string>();
            IsMoving = new List<string>();
        }

        public void PrintMovements()
        {
            Console.Write($"The {name} can: ");
            for (int i = 0; i < Movements.Count; i++)
            {
                Console.Write($"{Movements[i]}, ");
            }
        }

        public void PrintRandomMovement()
        {
            Random rnd = new Random();
            int index = rnd.Next(IsMoving.Count);
            Console.Write($"\nThe {name} is {IsMoving[index]}");
        }

        public void CheckMovements()
        {
            if (this.GetType().GetInterface("IFlyable") != null)
            {
                Movements.Add("fly");
                IsMoving.Add("flying");
            }

            if (this.GetType().GetInterface("IClimbable") != null)
            {
                Movements.Add("climb");
                IsMoving.Add("climbing");
            }

            if (this.GetType().GetInterface("IWalkable") != null)
            {
                Movements.Add("walk");
                IsMoving.Add("walking");
            }

            if (this.GetType().GetInterface("IRunnable") != null)
            {
                Movements.Add("run");
                IsMoving.Add("running");
            }
        }

        //IComparable method. Comparing names.
        public int CompareTo(Animal obj)
        {
            return this.name.CompareTo(obj.name);
        }
    }
}
