﻿using System;
using System.Data.SQLite;
using System.Data;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Linq;

namespace task19_chinook_db
{
    class Program
    {
        public static object Messagebox { get; private set; }

        static void Main(string[] args)
        {
            SQLiteConnection sqlite_conn;
            sqlite_conn = CreateConnection();
            Console.WriteLine("CustomerId, FirstName, LastName, InvoiceId");
            ReadAllFromCustomer(sqlite_conn);
            SaveCustomerToFile(sqlite_conn);
        }

        static SQLiteConnection CreateConnection()
        {

            SQLiteConnection sqlite_conn;
            // Create a new database connection:
            sqlite_conn = new SQLiteConnection(@"Data Source=  C:\Users\cthenn\Documents\experis\gitlab\accelerateexperistasks_week2\task19_chinook_db\chinook.db; Version = 3; New = True; Compress = True; ");
            // Open the connection:
            try
            {
                sqlite_conn.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return sqlite_conn;
        }

        static void ReadAllFromCustomer(SQLiteConnection conn)
        {
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = conn.CreateCommand();

            sqlite_cmd.CommandText = "SELECT customers.CustomerId, FirstName, LastName, invoices.InvoiceId" +
                " FROM customers" +
                " LEFT JOIN invoices ON invoices.CustomerId = customers.CustomerId; ";

            using (SQLiteDataReader reader = sqlite_cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        Console.Write(reader.GetValue(i) + ", ");

                    }
                    Console.WriteLine("\n");
                }
            }
        }

        static void SaveCustomerToFile(SQLiteConnection conn)
        {
            // First getting the number of customers (ids).
            SQLiteCommand sqlite_cmd1;
            sqlite_cmd1 = conn.CreateCommand();
            sqlite_cmd1.CommandText = "SELECT count(*) FROM customers; ";
            SQLiteDataReader reader2 = sqlite_cmd1.ExecuteReader();
            reader2.Read();

            // Converting query result and picking random customerId.
            int numcust = Convert.ToInt32(reader2[0]);
            Random rnd = new Random();
            string rndCustomer = rnd.Next(numcust).ToString();

            // Query for retrieving information about the customer with the random Id.
            SQLiteCommand sqlite_cmd2;
            sqlite_cmd2 = conn.CreateCommand();
            sqlite_cmd2.CommandText = $"SELECT CustomerId, FirstName, LastName FROM customers WHERE CustomerId = {rndCustomer}";

            // Query the invoices from the given random customer id.
            SQLiteCommand sqlite_cmd3;
            sqlite_cmd3 = conn.CreateCommand();
            sqlite_cmd3.CommandText = $"SELECT InvoiceId FROM invoices WHERE CustomerId = {rndCustomer}";

            
            List<object> customerObjects = new List<object>();
            Dictionary<string, object> record = new Dictionary<string, object>();

            // Executing and going through the records, saving each field to a -
            // dict and then saving the whole record as a object.
            using (SQLiteDataReader reader1 = sqlite_cmd2.ExecuteReader())
            {
                while (reader1.Read())
                {               
                    for (int i = 0; i < reader1.FieldCount; i++)
                    {
                        record.Add(reader1.GetName(i), reader1[i]);
                    }
                    customerObjects.Add(record);
                }
            }

            List<string> invoiceId = new List<string>();
            Dictionary<string, object> invoiceDict = new Dictionary<string, object>();

            // Executing and going through the InvoiceIds that is linked to the given customerId.
            // Adding the InvoiceIds collected in a collection.
            using (SQLiteDataReader reader3 = sqlite_cmd3.ExecuteReader())
            {
                while (reader3.Read())
                {
                    for (int i = 0; i < reader3.FieldCount; i++)
                    {
                        invoiceId.Add(reader3[i].ToString());
                    }
                }
            }

            // Adding all all the contents of invoiceID into the invoice dictionary.    
            invoiceDict.Add($"Invoice ID: ", invoiceId);
            // And now adding all the content of invoiceDict to the customerObject.
            customerObjects.Add(invoiceDict);
            string filename = record["FirstName"].ToString() + "_" + record["LastName"].ToString() + ".json";

            SaveAsFile(customerObjects, filename);
        }

        static void SaveAsFile(object obj, string filename)
        {
            //string json = string.Empty;
            string json = JsonConvert.SerializeObject(obj);
            using (StreamWriter sw = new StreamWriter(File.Create($@"C:\Users\cthenn\Documents\experis\gitlab\accelerateexperistasks_week2\task19_chinook_db\{filename}")))
            {
                string jsonFormatted = JValue.Parse(json).ToString(Formatting.Indented);
                sw.Write(jsonFormatted);
            }
        }
    }
}

